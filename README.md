# ADS-B Receiver version v1.0 for Raspberry Pi 3 Model B 

## Feature
*  Decode ข้อมูล ADS-B จากเสา 1090 Mhz
*  รับข้อมูลจาก dump1090 service 
*  ส่งข้อมูลผ่าน HTTP POST ไปที่ soar.gistda.or.th
*  ระบบสลับการทำงานเมื่อชุด a เกิดข้อผิดพลาด (redundancy)
*  ระบบสำรองข้อมูลภายในหน่วยความจำ(SD Card)สูงสุด 2.5 GB และจะทำการ clear ออก
*  ระบบบันทึก log การทำงานในหน่วยความจำสูงสุด 50 MB และจะทำการ clear ออก
*  ระบบส่งข้อมูลสถานะของอุปกรณ์ผ่าน MQTT Protocol

## การทำงานของระบบ
การทำงานของระบบจะแบ่งเป็น 5 ส่วน
1.  การอ่านและส่งข้อมูล
เริ่มจากอ่านข้อมูลทุก 1 วินาทีจาก dump1090 service ผ่าน HTTP GET ที่ [http://127.0.0.1:8080/data.json](127.0.0.1:8080/data.json) จากนั้นจะส่งข้อมูลไปที่ [http://soar.gistda.or.th:8080/putdata](http://soar.gistda.or.th:8080/putdata) ผ่าน HTTP POST
2.  การสำรองและล้างข้อมูล
เริ่มจากอ่านข้อมูลทุก 1 วินาทีและเก็บลง Array ไปเรื่อยๆ เมื่อครบ 1000 ตัวจะเขียนข้อมูลลง JSON ไฟล์โดยตั้งชื่อตามเวลา unixtime เช่น `data_1557125410.json` ซึ่งไฟล์จะมีขนาดโดยเฉลี่ยประมาณ 250 Kb เมื่อจำนวนไฟล์ใน `/home/pi/storage` มากกว่าหรือเท่ากับ 10000 ไฟล์ จะลบไฟล์ทั้งหมดทิ้ง ทั้งนี้ระบบสามารถสำรองข้อมูลได้ประมาณ 2.5 Gb
3.  การเก็บและล้าง log ไฟล์
เป็นระบบที่บันทึกข้อความการทำงานลงไฟล์ '/home/pi/logs/receiver.log' ซึ่งจะบันทึกทุก action ของระบบมี 3 ประเภท `warning` `debug` `info` เมื่อขนาด log ไฟล์เกิน 50 Mb จะทำการล้างข้อความทั้งหมดและเริ่มบันทึกใหม่
4.  การส่งสถานะอุปกรณ์
เป็นระบบที่ส่งสถานะปัจจุบันไปที่ server ผ่าน MQTT Protocol ซึ่งจะทำงานทุก 1 วินาทีโดยข้อความสถานะจะมี `อุณหภูมิ` `หมายเลขอุปกรณ์` `ชื่อรุ่นอุปกรณ์` `ชื่อรุ่นOS` `สถานะโหมด` `สถานะเวลา` `เวลา` `แรงดันไฟฟ้า` `จำนวนไฟล์สำรอง` `รหัสอุปกรณ์` `สถานะkernelล่าสุด`
5.  การสลับการทำงานเมื่อเกิดการผิดพลาด
เป็นระบบที่จะสลับการทำงานจากชุด a ไป b เมื่อชุด a ไม่ตอบสนองหรือไม่สามารถใช้งาน internet ได้ ซึ่งใช้การสื่อสารผ่าน UART ระหว่างชุด a และ b โดยจะส่งข้อความ `{ req : 0, res : 0, temp : 0, network : 0}` โดยชุด b จะส่งไปให้ a จากนั้น a จะนำค่า res มาบวกเพิ่ม 1 พร้อมกับตรวจสอบ internet หากใช้งานไม่ได้จะกำหนด network เป็น 0 และส่งกลับไปให้ b หาก b ได้รับ network เป็น 0 หรือไม่ได้รับ res เกิน 10 ครั้งชุด b จะเริ่มทำงานแทนชุด a เมื่อชุด a สามารถกลับมาทำงานได้ปกติ ชุด b จะหยุดการทำงานทันที

## การติดตั้งอุปกรณ์

**Under construction**

## การติดตั้ง software

### ขั้นตอนที่ 1 : Configure UART on Raspberry Pi

เริ่มจากทำการเปิด UART โดยใช้คำสั่ง
`sudo raspi-config` 
จากนั้นไปที่เมนู Interfacing Options -> Serial 
![step1](images/UARTstep1.png)
![step2](images/UARTstep2.png)

จากนั้นมันจะถามว่าต้องการให้สามารถ login ผ่าน serial ได้หรือไม่ ให้ตอบ No 
![step3](images/UARTstep3.png)

จากนั้นมันจะถามว่าต้องการเปิด Hardware Serial หรือไม่ ให้ตอบ Yes 
![step4](images/UARTstep4.png)

จากนั้นมันจะแจ้งผลการตั้งค่าให้กด Ok และทำการ reboot
![step5](images/UARTenabled.png)

picture ref. => https://www.electronicwings.com/raspberry-pi/raspberry-pi-uart-communication-using-python-and-c

### ขั้นตอนที่ 2 : download shell script โดย git

เริ่มจากการติดตั้ง git สามารถ download ผ่าน apt-get โดยใช้คำสั่ง 
`sudo apt-get install git` 
เมื่อติดตั้งเสร็จให้ไปที่ directory /home/pi จากนั้น download shell script โดยใช้คำสั่ง 
`git clone https://gitlab.com/supavit13/adsbreceiver.git`
หลังจาก clone เสร็จแล้ว ให้เข้าไปที่ adsbreceiver โดยใช้คำสั่ง 
`cd adsbreceiver`
แก้ไข .env ไฟล์ดังนี้
```
SERVER_PORT=8080 <---- port server
NODE=3  <---- หมายเลขอุปกรณ์
SUB_NODE=a <---- รหัสอุปกรณ์
SECRET=5cb6e06f12da4475f3888f4e <---- รหัส secret สำหรับ access server
KEY=oU6fgPiCPX <---- รหัส key สำหรับ access server
```
ทำการรันไฟล์ setup_adsbreceiver.sh โดนใช้คำสั่ง
`sudo ./setup_adsbreceiver.git`



```
sudo apt-get install git
cd /home/pi
git clone https://gitlab.com/supavit13/adsbreceiver.git
cd adsbreceiver
sudo ./setup_adsbreceiver.sh
```
### ขั้นตอนที่ 3 : การสร้างและsign client certificate สำหรับ openvpn ให้กับอุปกรณ์
เริ่มจาก ssh ไปที่ soar digitalocean จากนั้นเข้า path ของตัวสร้าง client certificate สำหรับ openvpn
```
/etc/openvpn/vpns/EasyRSA-3.0.4/
```
จากนั้นทำการสร้าง certificate request ใช้คำสั่ง `sudo ./easyrsa gen-req <clientName> nopass` ให้แทน `<clientName>` ด้วยชื่อของอุปกรณ์ที่เราต้องการ เช่น
```
sudo ./easyrsa gen-req client1 nopass
```
กด Enter เพื่อ confirm ชื่อเดิมที่เราตั้งไว้ เราจะได้ไฟล์ certificate request  จากนั้นทำการ sign certificate reqeust โดยใช้คำสั่ง `sudo ./easyrsa sign-req client <clientName>` ให้แทน `<clientName>` ด้วยชื่อเดียวกับในขั้นตอนการสร้าง certificate request เช่น
```
sudo ./easyrsa sign-req client client1
```
ตอบ `yes` เพื่อยืนยันการ sign certificate request เราจะได้ไฟล์ certificate มา อยู่ใน path `pki/issued` และ `pki/private` เช่น
```
pki/issued/client1.crt
pki/private/client1.crt
```
ให้ copy สองไฟล์นี้ไปที่ path `/etc/openvpn/vpns/client-configs/keys/ โดยใช้คำสั่ง
```
sudo cp pki/issued/client1.crt ../client-configs/keys
sudo cp pki/private/client1.crt ../client-configs/keys
```
ขั้นตอนการสร้าง client configurations ไฟล์เพื่อนำไปใช้ในการเชื่อมต่อ vpn โดยให้ไปที่ path ของตัวสร้าง configurations ไฟล์
```
cd /etc/openvpn/vpns/client-configs
```
ทำการสร้าง configurations ไฟล์โดยใช้คำสั่ง `sudo ./make_config.sh <clientName>` ให้แทน `<clientName>` ด้วยชื่อเดียวกับในขั้นตอนการสร้าง certificate request เช่น
```
sudo ./make_config.sh client1
```
เมื่อ shell script ทำงานเสร็จจะได้ไฟล์ `client.ovpn` ใน path
```
cd /etc/openvpn/vpns/client-configs/files/
```

### ขั้นตอนที่ 4 : การใช้ติดตั้งและใช้งาน configuration สำหรับ openvpn
เริ่มจาก ssh เข้า raspberry pi จากนั้นให้ download configuration ไฟล์จากขี้นตอนที่แล้วโดยใช้คำสั่ง `scp <user>@<server>:/path/to/ovpn/files /etc/openvpn/` ให้แทน `<user>` `<server>` ด้วยชื่อ user และ ip หรือ domain ของ server เช่น
```
scp root@192.168.0.1:/etc/openvpn/vpns/client-configs/files/client1.ovpn /etc/openvpn/
```
จากนั้นเพิ่ม cronjob เพื่อให้เวลา raspberry pi reboot จะสามารถเปิด openvpn อัตโนมัติ
```
(crontab -l; echo "@reboot sudo openvpn /etc/openvpn/client1.ovpn") | crontab -
```
ทำการเปิด openvpn
```
sudo openvpn /etc/openvpn/client1.ovpn 
```
เช็ค IP ดูที่ tun
```
ifconfig
```
### ขั้นตอนที่ 5 : การปรับตั้งค่า Linux 
แม้ระบบไฟจะสมบูรณ์ แต่บางครั้งก็เลี่ยงไฟดับไม่ได้ ความผิดพลาดที่มักเกิดขึ้นเมื่อ Raspberry Pi ดับโดยไม่ Shutdown คือ disk จะอยู่ในสถานะที่ไม่เสถียร ไฟล์อาจมีปัญหา เมื่อระบบตรวจ disk ตอน boot แล้วพบว่ามีปัญหาก็จะเข้าสู่ emergency mode ทำให้ไม่รันโปรแกรมใดๆ ผู้ดูแลต้องเข้ามาแก้ไขเอง แล้ว boot ใหม่ระบบจึงจะทำงานตามปกติ ขั้นตอนต่อไปนี้น่าจะช่วยแก้ไขสถานการณ์นี้ได้
1. แก้ /boot/cmdline.txt ให้บังคับตรวจ disk และแก้ไขข้อผิดพลาดโดยอัตโนมัติ ให้เพิ่มสองคำสั่งนี้ต่อท้ายไฟล์ /boot/cmdline.txt (อย่าขึ้นบรรทัดใหม่ ให้ใช้วรรคแบ่งคำสั่ง)
    ```
    fsck.mode=force  fsck.repair=yes
    ```
2. เปลี่ยน I/O mode ของ disk จาก async เป็น sync ในไฟล์ /etc/fstab ให้เพิ่มพารามิเตอร์ sync เข้าไปที่ root partition ดังตัวอย่าง
    ```
    /dev/mmcblk0p2     /        ext4      defaults,noatime,sync     0             1
    ```
โดยปกติแล้ว I/O mode จะเป็น async ซึ่งแม้จะทำงานได้เร็วกว่าแต่ก็เกิดปัญหาได้ง่ายกว่า เพราะการที่ async ชลอการกระทำ I/O ออกไปจนกว่าจะถึงเวลาที่เหมาะสม ทำให้ข้อมูลบน disk มักไม่ตรงกับข้อมูลจริง เมื่อเกิดไฟดับ โอกาสที่ไฟล์จะมีปัญหาก็จะสูงขึ้น กรณีนี้คล้ายกับเวลาเราดึง flash drive ออกจากคอมพิวเตอร์โดยไม่ได้ eject ก่อนให้ดี ไฟล์ที่ copy ใส่ flash drive อาจยังถูกเขียนไม่เสร็จสิ้นและส่งผลให้ไฟล์เหล่านั้นเสียได้

หมายเหตุ: การกระทำกับไฟล์อาจจะช้าลงเมื่อใช้โหมด sync หากโครงงานของท่านต้องมีการอ่านเขียนกับ disk มากๆ อาจต้องทดสอบดูก่อนว่าเกิดผลกระทบต่อประสิทธิภาพและความเร็วมากเพียงใดประกอบการตัดสินใจ แต่เท่าที่ผมลองดูเองกับการใช้งานทั่วๆ ไป ระบบก็ไม่ได้ช้าลงถึงขั้นสังเกตได้

ref. => https://learninginventions.org/?p=2626&fbclid=IwAR10X6EeYkAzIHdLxxi2hDgiJKzRcwSIk724riAut5t1hoEysJhyDH5fUso



## How work?
### split 4 threads
```
└── main.py
    ├── receiver.py
    ├── logger.py
    ├── collector.py
    └── reporter.py
```
### main.py
```
├── inital variable
├── get 4 classes
├── create 4 threads
├── run 4 threads
└── wait exception
```
### receiver.py
```
├── get data every 1 second from dump1090 in localhost:8080/data.json
├── assign unixtime and node_number in data
└── attach data and API_KEY in payload and send to server by http post method
```
### reporter.py
```
├── connect to mqtt server
├── subscribe topic for receiving command from server
├── mqtt start
├── get status
│   ├── temperature
│   ├── node_number
│   ├── hardware_version
│   ├── os_version
│   ├── online status
│   ├── timestate
│   ├── voltage
│   ├── current date
│   └── files in directory /home/pi/backup
└── send status
```
### collector.py
```
├── extend data to temp_data
├── if files in directory /home/pi/storage over 10k
|   ├── remove all files in the directory
└── if temp_data size over 1000
    ├── write file data_{unixtime}.json
    └── clear temp_data
```
### logger.py
```
├── if log_file exist
|   ├── change filemode to append
├── else 
|   ├── change filemode to writing
└── if log_file size over 50 Mb
    ├── clean log_file
    └── change filemode to writing
```
