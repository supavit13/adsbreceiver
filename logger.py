import os
import logging
import os
from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
class Logger:
	def __init__(self):
		self.path = os.getenv("LOG_FILE")
		self.check_file_exist()
		self.clear_log_file()
	def check_file_exist(self):
		if os.path.isfile(self.path):
			logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(levelname)s %(message)s',
                                filename=self.path,
                                filemode='a')
			logging.debug('ADS-B Receiver starting...')
			logging.debug('File mode : appending')
		else:
			logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(message)s',
                        filename=self.path,
                        filemode='w')
			logging.debug('ADS-B Receiver starting...')
			logging.debug('File mode : writing')
	def log_writer(self,msg,log_type):
		if log_type == "warning":
			logging.warning(msg)
		elif log_type == "debug":
			logging.debug(msg)
		elif log_type == "info":
			logging.info(msg)

	def clear_log_file(self):
		if os.path.isfile(self.path) and os.path.getsize(self.path) >= 50000000:
			open(self.path, "w").close()
			logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(levelname)s %(message)s',
                                filename=self.path,
                                filemode='w')
			logging.debug('ADS-B Receiver starting...')
			logging.debug('File mode : clearing')

