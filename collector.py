import json
import os
from time import time,sleep
class Collector:
	def __init__(self):
		self.temp_data = []
		self.filenumber = 0
	def check_file_dir(self):
		files = [f for f in os.listdir('/home/pi/storage/') if os.path.isfile(os.path.join('/home/pi/storage/',f))]
		return len(files)
	def write_to_file(self,data):
		self.temp_data.extend(data)
		#self.filenumber = self.check_file_dir()
		self.filenumber = int(time())
		if(len(self.temp_data) >= 1000):
			try:
				with open('/home/pi/storage/data_'+str(self.filenumber)+'.json','w') as raw:
					json.dump(self.temp_data,raw)
					self.temp_data = [];
					print(len(self.temp_data))
			except:
				pass
	def file_over_10k(self):
		if self.check_file_dir() >= 10000:
			o = os.popen("sudo rm -f /home/pi/storage/data_*").read()
