import time
import serial
import json
import threading

class GPSControl:
	def __init__(self,port="/dev/ttyUSB1",baudrate=9600):
		self.ready = True
		try:
			self.NMEAinf = serial.Serial(port,baudrate,timeout=5,rtscts=True,dsrdtr=True)
		except Exception as e:
			self.ready = False
			
		self.gps_data = ""
		self.coordinate = {"lat":0.0,"lon":0.0}
	def gps_readline(self):
		try:
			self.gps_data = self.NMEAinf.readline()
			dataArray = self.gps_data.split(',')
			if dataArray[0] == "$GPRMC":
				self.ddm_to_dd(dataArray)
		except Exception as e:
			print e
	def ddm_to_dd(self,dataArray):
		degreeLat = int(float(dataArray[3])/100)
		degreeLon = int(float(dataArray[5])/100)
		minLat = float(dataArray[3]) - float(degreeLat*100)
		minLon = float(dataArray[5]) - float(degreeLon*100)
		self.coordinate['lat'] = float(degreeLat+(minLat/60.0))
		self.coordinate['lon'] = float(degreeLon+(minLon/60.0))
	def gps_ready(self):
		return self.ready
	def get_coordinate(self):
	    return self.coordinate
	def gps_update(self):
		while 1:
			self.gps_readline()

