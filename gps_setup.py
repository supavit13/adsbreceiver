import serial 
class GPSSetup:
	def __init__(self):
		self.atinf = serial.Serial("/dev/ttyUSB2",115200,timeout=5,rtscts=True,dsrdtr=True)
		self.enableGNSS()
		self.enableGNSSAuto()
	def read_serial(self):
		res = self.atinf.read(100)
		return res
	def write_serial(self,msg):
		self.atinf.write(b""+msg+"\r")
	def checkAT(self):
		self.write_serial("AT")
		res = self.read_serial()
		return res.split('\n')[1] == "OK\r"
	def enableGNSS(self):
		if self.checkAT():
			self.write_serial("AT+QGPS=1")
			res = self.read_serial()
			print res
		else:
			print "check AT Failed"
	def enableGNSSAuto(self):
		if self.checkAT():
			self.write_serial('AT+QGPSCFG="autogps",1')
			res = self.read_serial()
			print res
		else:
			print "check AT Failed"

gs = GPSSetup()

