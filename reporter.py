import sys
from time import time,sleep
from datetime import datetime
import thread
import os
from os.path import join, dirname
from dotenv import load_dotenv
import random
import paho.mqtt.client as mqtt 
import json
import re
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


class Reporter:
	def __init__(self):
		self.client = mqtt.Client(client_id="client_id_"+str(random.randint(0,10000000)),transport="websockets")
		self.payload = {"request":0,"response":0 , "temporary" : 0}
		self.client.on_connect = self.on_connect
		self.client.on_message = self.on_message
		self.client.on_disconnect = self.on_disconnect
		self.client.tls_set()
		self.data = {
			"temperature" : 0,
			"node_number" : os.getenv("NODE"),
			"hardware_version" : "initial setup",
			"os_version" : "initial setup",
			"status" : "initial setup",
			"volts" : 0,
			"sub_node" : "initial setup",
			"date" : "initial setup",
			"lastlog" : "initial setup",
			"timestate" : "initial setup",
			"backupfiles" : 0
			
		}
		o = os.popen("cat /proc/device-tree/model").read()
		self.data['hardware_version'] = o.split('\u0000')[0]
		o = os.popen("lsb_release -d").read()
		o = re.sub("(Description:\s*)",'',o)
		o = re.sub("\n",'',o)
		self.data['os_version'] = o
		self.data['sub_node'] = os.getenv("SUB_NODE")
		try:
			self.client.connect(os.getenv("MQTT_ADDRESS"),int(os.getenv('MQTT_PORT')),60)
		except:
			print "connection lost"
		self.client.subscribe("Node/"+os.getenv("NODE")+"/request/command")
		self.client.subscribe("Node/"+os.getenv("NODE")+"/sub/"+os.getenv("SUB_NODE"))
		#self.client.loop_start()
	def on_connect(self,client,userdata,rc,result):
		pass
	def on_message(self,client,userdata,msg):
		cmd = msg.payload.decode('utf-8')
		if msg.topic == "Node/"+os.getenv("NODE")+"/request/command":
			if os.getenv("SUB_NODE") == "b" and self.payload["temporary"] != 0 and self.payload["request"] - self.payload["response"] >= 10:
				o = os.popen(cmd).read()
				self.client.publish("Node/"+os.getenv("NODE")+"/response/command",o)
			elif os.getenv("SUB_NODE") == "a":
				o = os.popen(cmd).read()
				self.client.publish("Node/"+os.getenv("NODE")+"/response/command",o)
		elif msg.topic == "Node/"+os.getenv("NODE")+"/sub/a":
			jsonObj = json.loads(cmd)
			self.client.publish("Node/"+os.getenv("NODE")+"/sub/b",jsonObj['response']+1)
		elif msg.topic == "Node/"+os.getenv("NODE")+"/sub/b":
			self.payload["response"] = int(cmd)
	def on_disconnect(self,client,userdata,rc):
		if rc != 0:
			pass
	def sending(self,data):
		self.client.publish("Node/Status",data)
	def log_publish(self,data):
		self.client.publish("Node/"+os.getenv("NODE")+"/sub/"+os.getenv("SUB_NODE")+"/lastlog",data)
	def on_checking(self):
		if self.payload["request"] - self.payload["response"] >= 10 and self.payload["temporary"] == 0:
			o = os.popen("service adsbreceiver start").read()
			self.payload["temporary"] = self.payload["response"]+1
		elif self.payload["temporary"] != 0 and self.payload["temporary"] <= self.payload["response"]:
			self.payload = {"request":0,"response":0 , "temporary" : 0}
			o = os.popen("service adsbreceiver stop").read()
		elif self.payload["request"] < self.payload["response"] :
			self.payload = {"request":0,"response":0 , "temporary" : 0}
			o = os.popen("service adsbreceiver stop").read()
		self.client.publish("Node/"+os.getenv("NODE")+"/sub/a",json.dumps(self.payload))
		self.payload["request"] = self.payload["request"]+1
	def get_payload(self):
		payload = str(self.data['temperature'])+"|"+self.data['node_number']+"|"+self.data['hardware_version']+"|"+self.data['os_version'] + "|"+str(self.data['status'])+"|"+str(self.data['timestate']) + "|"+self.data['date']+"|"+str(self.data['volts'])+"|"+str(self.data['backupfiles'])+"|"+self.data['sub_node']+"|"+self.data['lastlog']
		#self.log_publish(self.data['lastlog'])
		return payload
	def get_status(self):
		volts = 0
		payload = ""
		o = os.popen("vcgencmd measure_temp").read()
		o = re.sub("[temp=C\'\n]",'',o)
		self.data['temperature'] = float(o)
		o = os.popen("sudo service adsbreceiver status").read()
		match = re.search("active\s\(running\)",o)
		if match:
			self.data['status'] = 1
		else:
			self.data['status'] = 0
		o = os.popen("timedatectl").read()
		match = re.search("\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\s\UTC",o)
		self.data['date'] = match.group()
		o = o.split('\n')[5]
		o = o.split(':')[1]
		if(o == " yes"):
			self.data['timestate'] = 1
		elif(o == " no"):
			self.data['timestate'] = 0
		o = os.popen("vcgencmd measure_volts core").read()
		o = re.sub("[volt=V\n]",'',o)
		volts = volts + float(o)
		o = os.popen("vcgencmd measure_volts sdram_c").read()
		o = re.sub("[volt=V\n]",'',o)
		volts = volts + float(o)
		o = os.popen("vcgencmd measure_volts sdram_i").read()
		o = re.sub("[volt=V\n]",'',o)
		volts = volts + float(o)
		o = os.popen("vcgencmd measure_volts sdram_p").read()
		o = re.sub("[volt=V\n]",'',o)
		volts = volts + float(o)
		self.data['volts'] = volts
		self.data['lastlog'] = os.popen("cat /var/log/kern.log | tail").read()
		files = [f for f in os.listdir('/home/pi/backup/') if os.path.isfile(os.path.join('/home/pi/backup/',f))]
		self.data["backupfiles"] = len(files)
		#payload += "|" + data['lastlog'] 

		
