import sys
from time import time,sleep
from datetime import datetime
import thread
import threading
import os
from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
from reporter import Reporter
from logger import Logger
from serialcom import SerialCom

#from gpscontrol import GPSControl


logger = Logger()
#gc = GPSControl()

def get_report(report):
    report.get_status()
    output = report.get_payload()
    print(output)
    #if gc.gps_ready():
	#crd = gc.get_coordinate()
	#output += "|"+str(crd["lat"])+","+str(crd["lon"])
    report.sending(output)
def get_sub_node():
    return os.getenv("SUB_NODE")
def on_change_state(ser):
    o = os.popen("sudo service adsbreceiver status").read()
    if ser.machine_state == 1 and ser.payload['network'] == 0 and o.find("active (running)") == -1:
	print "sudo service adsbreceiver start"
	o = os.popen("sudo service adsbreceiver start").read()
	logger.log_writer("reporter : network down","warning")
    elif ser.machine_state == 1 and ser.payload['network'] == 1 and o.find("active (running)") > 0:
	print "sudo service adsbreceiver stop"
	o = os.popen("sudo service adsbreceiver stop").read()
	logger.log_writer("reporter : machine start","warning")
    elif ser.machine_state == 0 and o.find("active (running)") == -1:
	print "sudo service adsbreceiver start"
	o = os.popen("sudo service adsbreceiver start").read()
	logger.log_writer("reporter : machine down","warning")
    
def sender_thread(ser):
    while 1 and get_sub_node() == "b":
	sleep(1)
	sendThreadObj = {'req':ser.payload['req'],'res':ser.payload['res'],'temp':ser.payload['temp']}
	ser.writeJSON(sendThreadObj)
	ser.payload['req']+=1
def check_payload(ser):
    if ser.payload['temp'] <= ser.payload['res'] and ser.payload['temp'] != 0:
	ser.payload['req'] = 0
	ser.payload['res'] = 0
	ser.payload['temp'] = 0
	ser.machine_state = 1
    elif ser.payload['req'] - ser.payload['res'] >= 10 and ser.payload['temp'] == 0:
	ser.payload['temp'] = ser.payload['res']+1
	ser.machine_state = 0
    on_change_state(ser)
	
def receiver_thread(ser):
    while 1:
	msg = ser.readSerial()
	if msg > 0 and msg != "":
	    if get_sub_node() == "b":
		print msg
		obj = ser.toJSON(msg)
		ser.payload['res'] = obj['res']
		ser.payload['network'] = obj['network']
	    elif get_sub_node() == "a":
		obj = ser.toJSON(msg)
		recv = {'req':obj['req'],'res':obj['res'],'temp':obj['temp'],'network':1}
		print recv
		ser.writeBack(recv)
	if get_sub_node() == "b":
	    check_payload(ser)
def send_report_thread(report,ser):
    while 1:
	sleep(1)
	if get_sub_node() == "a":
	    get_report(report)
	elif get_sub_node() == "b" and (ser.machine_state == 0 or ser.payload['network'] == 0):
	    get_report(report)

def check_internet_thread(ser):
    while 1:
	sleep(1)
	if get_sub_node() == "a" :
	    if ser.is_online():
		print "online"
		ser.payload["network"] = 1
	    else:
		print "offline"
		ser.payload["network"] = 0
	
	
if __name__ == "__main__":
    report = Reporter()
    ser = SerialCom()
    if get_sub_node() == "b":
	o = os.popen("sudo service adsbreceiver stop").read()
    try:
	thread0 = threading.Thread(target=sender_thread , args=(ser,))
	thread1 = threading.Thread(target=receiver_thread , args=(ser,))
	thread1_1 = threading.Thread(target=report.client.loop_forever)
	thread2 = threading.Thread(target=send_report_thread , args=(report,ser,))
	thread2_2 = threading.Thread(target=report.get_status)
	thread3 = threading.Thread(target=check_internet_thread , args=(ser,))
	#thread4 = threading.Thread(target=report.client.loop_forever)
	#if gc.gps_ready():
	    #thread5 = threading.Thread(target=gc.gps_update)
	thread0.start()
	thread1.start()
	thread1_1.start()
	thread2.start()
	thread2_2.start()
	thread3.start()
	#thread4.start()
	#if gc.gps_ready():
	    #thread5.start()
	thread0.join()
	thread1.join()
	thread1_1.join()
	thread2.join()
	thread2_2.join()
	thread3.join()
	#thread4.join()
	#if gc.gps_ready():
	    #thread5.join()
    except:
	logger.log_writer("Thread exception in main_reporter.py","warning")
	
