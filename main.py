import sys
from time import time,sleep
from datetime import datetime
import thread
import threading
import os
from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
from receiver import Receiver
from reporter import Reporter
from logger import Logger
from collector import Collector

logger = Logger()

def on_trx_data(recv):
    while True:
        sleep(1)
        output = recv.get_data()
    	output = recv.send_data(output)
        if output == None:
            logger.log_writer("on_trx_data : except","debug")
        else:
            logger.log_writer("on_trx_data : done","debug")
def on_report_status(report):
    while True:
        sleep(1)
        output = report.get_status()
        report.sending(output)
        logger.log_writer("on_report_status : done","debug")
def on_clean_log(logger):
    while True:
        sleep(1)
        logger.clear_log_file()
        logger.log_writer("on_clean_log : done","debug")
def on_collect_data(recv,collector):
    while True:
        sleep(1)
        output = recv.get_data()
        collector.write_to_file(output)
        logger.log_writer("on_collect_data : done","debug")
def clean_collect_data(collector):
    while True:
        sleep(1)
        collector.file_over_10k()
        logger.log_writer("clean_collect_data : done","debug")

if __name__ == "__main__":
    url = os.environ.get("SERVER_ADDRESS")
    port = os.getenv("SERVER_PORT")
    node = int(os.getenv("NODE"))
    key = os.getenv("KEY")
    secret = os.getenv("SECRET")
    recv = Receiver()
    recv.set_env(url,port,node,key,secret)
    #report = Reporter()
    collector = Collector()
    try:
        thread1 = threading.Thread(target=on_trx_data, args=(recv,))
        thread2 = threading.Thread(target=on_collect_data, args=(recv,collector,))
        thread3 = threading.Thread(target=clean_collect_data, args=(collector,))
        #thread4 = threading.Thread(target=on_report_status, args=(report,))
        thread5 = threading.Thread(target=on_clean_log, args=(logger,))
        thread1.start()
        thread2.start()
        thread3.start()
        #thread4.start()
        thread5.start()
        thread1.join()
        thread2.join()
        thread3.join()
        #thread4.join()
        thread5.join()
        #thread.start_new_thread(on_trx_data,(recv,))
        #thread.start_new_thread(on_collect_data,(recv,collector,))
        #thread.start_new_thread(clean_collect_data,(collector,))
        #thread.start_new_thread(on_report_status,(report,))
        #thread.start_new_thread(on_clean_log,(logger,))
    except:
        logger.log_writer("Thread exception in main.py","warning")
    #while True:
    #    pass
