import requests
import time 
import serial 
import os
import json
from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

class SerialCom:
	def __init__(self):
		self.ser = serial.Serial(port="/dev/ttyS0",baudrate = 115200,parity = serial.PARITY_NONE,stopbits = serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1)
		self.status = "Normal"
		self.payload = {'req':0,'res':0,'temp':0,'network':1}
		self.machine_state = 1
	def readSerial(self):
		x = self.ser.readline()
		cmd = x.split('\n')[0]
		return cmd
	def writeBack(self,obj):
		obj['res'] += 1
		obj['network'] = self.payload['network']
		self.writeJSON(obj)
	def writeSerial(self,cmd):
		self.ser.write(cmd+"\n")
	def writeJSON(self,obj):
		msg = json.dumps(obj)
		self.ser.write(msg+"\n")
	def toJSON(self,msg):
		obj = json.loads(msg)
		return obj
	def is_online(self):
		r = None
		try:
			r = requests.get("http://soar.gistda.or.th:8080",verify=1,timeout=1)
		except:
			pass
		return str(r) != "None"
