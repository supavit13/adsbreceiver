import os 
import re


o = os.popen("sudo cat /home/pi/adsbreceiver/.env").read()
match = re.search("NODE=\d",o)
Node = re.sub("[NODE\=]","",match.group())
match = re.search("SUB_NODE=\w",o)
SubNode = re.sub("[SUB_NODE\=]","",match.group())
#check umtskeeper running
o = os.popen("ps ax | grep umtsk").read()
match = re.search("sudo /home/pi/umtskeeper/umtskeeper",o)
if not match:
	os.popen("sudo /etc/rc.local")
#check service adsbreporter status
o = os.popen("sudo service adsbreporter status").read()
match = re.search("active\s\(running\)",o)
if not match:
	os.popen("sudo service adsbreporter restart")
#check openvpn running
o = os.popen("ps ax | grep openvpn").read()
match = re.search("sudo openvpn /etc/openvpn/node",o)
if not match:
	os.popen("sudo openvpn /etc/openvpn/node"+Node+SubNode+".ovpn &")

