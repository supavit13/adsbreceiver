import requests
import json
from time import time,sleep
import os 
from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

class Receiver:
	def __init__(self):
		self.header = {"Content-Type": "application/json","Accept" : "application/json"}
		self.URL = ""
		self.API_KEY = ""
		self.NODE = int(os.getenv("NODE"))
	def set_env(self,url=None,port=None,node=None,key=None,secret=None):
		self.URL = "http://"+url+":"+port+"/putdata"
		self.API_KEY = key+"@"+secret
	def get_data(self):
		try:
			res = requests.get(os.getenv("DATA_ADDRESS"),verify=False)
			data = res.json()
		except:
			data = {}
		for idx,raw in enumerate(data):
			raw['node_number'] = self.NODE
			raw['unixtime'] = int(time())
			data[idx] = raw
		return data
	def send_data(self,data):
		try:
			res = requests.post(url = self.URL,json = {'auth':self.API_KEY,'data':data},headers=self.header,verify=False)
		except:
			res = None
		return res

