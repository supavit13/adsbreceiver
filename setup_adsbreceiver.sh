#!/bin/bash
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@ Update & Install @@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-pip git -y
sudo apt-get install cmake libusb-1.0-0-dev librtlsdr-dev rtl-sdr -y
sudo apt-get install connectd
sudo apt-get install usb-modeswitch ppp -y
sudo apt-get install openvpn -y
rtl_test -t
cd /home/pi

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@ Install dump1090 @@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

git clone https://github.com/K2DLS/dump1090.git
cd dump1090
make
sudo make install
echo 'DUMP1090_OPTS="--quiet --net --ppm 19 --gain -10"' > /etc/default/dump1090
sudo systemctl enable dump1090.service
sudo systemctl start dump1090.service

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@@ Watchdog setup @@@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

sudo modprobe bcm2835_wdt
echo "bcm2835_wdt" | sudo tee -a /etc/modules
sudo apt-get install watchdog
sudo update-rc.d watchdog defaults
echo "max-load-1 = 24" >> /etc/watchdog.conf
sudo service watchdog start

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@@@@ UART setup @@@@@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

echo "enable_uart=1" >> /boot/config.txt
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service


echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@ Install python library @@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

pip install -r /home/pi/adsbreceiver/requirements.txt
mkdir /home/pi/backup
mkdir /home/pi/logs
mkdir /home/pi/storage
cd /home/pi/adsbreceiver

#echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
#echo "@@@@@@@ GPS interface setup @@@@@@@@"
#echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

#sudo python gps_setup.py

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@ ADS-B service setup @@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

cp adsbreceiver.service /lib/systemd/system/adsbreceiver.service
cp adsbreporter.service /lib/systemd/system/adsbreporter.service
sudo cp rc.local /etc/rc.local
sudo chmod 755 /etc/rc.local
chmod 644 /lib/systemd/system/adsbreceiver.service
chmod 644 /lib/systemd/system/adsbreporter.service
systemctl daemon-reload
systemctl enable adsbreceiver.service
systemctl start adsbreceiver.service
systemctl enable adsbreporter.service
systemctl start adsbreporter.service

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@ 3G Shield setup @@@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

cd /home/pi
mkdir umtskeeper
cd umtskeeper
wget "http://mintakaconciencia.net/squares/umtskeeper/src/umtskeeper.tar.gz"
tar -xzvf umtskeeper.tar.gz
chmod +x sakis3g umtskeeper

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@@ Scheduler task @@@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

(crontab -l; echo "@reboot sudo /etc/rc.local") | crontab -
(crontab -l; echo "@reboot sudo service adsbreceiver restart && sudo service adsbreporter restart") | crontab -
(crontab -l; echo "@reboot sudo openvpn /etc/openvpn/node3a.ovpn &") | crontab -
(crontab -l; echo "* * * * * sudo python /home/pi/adsbreceiver/systemcheck.py") | crontab -
(crontab -l; echo "0 2 * * * sudo service adsbreceiver restart && sudo service adsbreporter restart") | crontab -

sudo service cron restart

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@@@@@@@ Remot3.it setup @@@@@@@@@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
sleep 2
sudo connectd_installer


